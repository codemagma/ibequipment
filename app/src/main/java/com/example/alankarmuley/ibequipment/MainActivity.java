package com.example.alankarmuley.ibequipment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private ImageButton btn;
    private Button btnLogout;
    private Switch btnSwitch;
    private ImageView iv_loading;
    private ImageView iv_unloading;
    private TextView tv_id;
    private TextView tv_unloading;
    private TextView tv_loading;
    private LinearLayout ll_loading_switch_background;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv_id = (TextView)findViewById(R.id.tv_id);
        btnLogout = (Button) findViewById(R.id.btn_logout);
        btn=(ImageButton)findViewById(R.id.ib_button);
        btnSwitch = (Switch)findViewById(R.id.switchBtn_main);
        iv_loading = (ImageView)findViewById(R.id.iv_loading);
        iv_unloading = (ImageView)findViewById(R.id.iv_unloading);
        tv_unloading = (TextView) findViewById(R.id.tv_unloading);
        tv_loading = (TextView) findViewById(R.id.tv_loading);
        ll_loading_switch_background =(LinearLayout )findViewById(R.id.ll_loading_switch_background);


        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Creating alert Dialog with one Button
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);


                //AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();

                // Setting Dialog Title
                alertDialog.setTitle("IBEquipment");

                // Setting Dialog Message
               // alertDialog.setMessage("Enter Password");

                //alertDialog.setView(input);

                final EditText input = new EditText(MainActivity.this);
                input.setHint("Operator Code ");
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);
                input.setLayoutParams(lp);
                alertDialog.setView(input); // uncomment this line

                // Setting Icon to Dialog

                // Setting Positive "Yes" Button
                alertDialog.setPositiveButton("Confirm",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int which) {
                                // Write your code here to execute after dialog
                                tv_id.setText("Supervisor logged in with id : " + input.getText());
                            }
                        });
                // Showing Alert Message
                alertDialog.show();
            }
        });
        btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent i = new Intent(getApplicationContext(),IBEquipment.class);
                startActivity(i);
            }
        });
        //set the switch to ON
        btnSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    iv_loading.setVisibility(View.VISIBLE);
                    tv_loading.setVisibility(View.VISIBLE);
                    ll_loading_switch_background.setBackgroundResource(R.color.green);
                    iv_unloading.setVisibility(View.INVISIBLE);
                    tv_unloading.setVisibility(View.INVISIBLE);
                }else {
                    iv_loading.setVisibility(View.INVISIBLE);
                    tv_loading.setVisibility(View.INVISIBLE);
                    ll_loading_switch_background.setBackgroundResource(R.color.red);
                    iv_unloading.setVisibility(View.VISIBLE);
                    tv_unloading.setVisibility(View.VISIBLE);
                }
            }
        });
    }
}
